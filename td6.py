#Exo 1.1 

def moy_pos (liste_nombres) :
    somme = 0 #additionne à lui meme chauqe valeur positif à chaque itération
    cpt = 0 #ajoute 1 à chaque itération ou la valeur est positif 
    ind = 0 #est l'indice, ajoute 1 a chaque itération
    while ind < len(liste_nombres) : #tans que ind est strictement inferieur à la taille de la liste
        if liste_nombres[ind] >= 0 : #si liste_nombres à lindice ind est strictement superieur ou egale à zéro 
            somme += liste_nombres[ind]
            cpt += 1
        ind+= 1
    if cpt == 0 : # si cpt est égale à zéro 
        return None 
    return somme/cpt 
#print (moy_pos([-7,-5,-6,-8]))

def test_moy_pos () :
    assert moy_pos([1,7,5,-6,-2]) == 4.333333333333333
    assert moy_pos ([]) == None
    assert moy_pos ([-7,-5,-6,-8]) == None

#Exo 1.1

def est_triee (liste_per) :
    ind = 1  #est l'indice, ajoute 1 a chaque itération
    res = True 
    if len(liste_per) != 0 :
        prec = liste_per[0][1]
        while ind < len(liste_per) : #tans que ind est strictement inferieur à la taille de la liste
            if liste_per[ind][1] > prec : #Si liste_per à lindice ind et la poistion 1 et strictement superieur a la val prec 
                res = False
            ind += 1 
            prec = liste_per[ind-1][1]
    return res
print(est_triee([('a',18),('b',27),('c',18)]))

def test_est_triee () :
    assert est_triee([('a',18),('b',27),('c',18)]) == False
    assert est_triee ([('a',45),('b',27),('c',18)]) == True 
    assert est_triee ([]) == True 


def est_palindrome (chaine) :
    ind = 0  #est l'indice, ajoute 1 a chaque itération
    res = True 
    while ind < (len(chaine)//2): #tans que ind est strictement inferieur à la moitié de la taille de la liste
        if chaine[ind]!= chaine[len(chaine)-1-ind] : #Si chaine à lindice ind est different de la taille de la chaine -1 -ind 
            res = False
        ind+=1
    return res
print(est_palindrome('lavbl'))
    
def test_est_palindrome () :
    assert est_palindrome('laval') == True
    assert est_palindrome('') == True
    assert est_palindrome('lavbl') == False